''' Assume s is a string of lower case characters.

Write a program that prints the number of times the string 'bob' occurs in s.
For example, if s = 'azcbobobegghakl', then your program should print
Number of times bob occurs is: 2
'''


def checker(s, m):
    ''' Function to check if m substring is in s,
        return the number of times m occurs in s
    '''
    # window which we'll move through the word
    window = len(m)                     # for bob, window = 3
    count = 0                           # keep track of count
    for i,e in enumerate(s):
        # using python slicing for moving the window by one step
        if s[i:i+window] == 'bob':
            count += 1
    return 'Number of times {word} occurs is: {count}'.format(word=m, count=count)

if __name__ == '__main__':
    print checker('azcbobobegghakl', 'bob')
