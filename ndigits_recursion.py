'''
Write a function called ndigits, that takes an integer x (either positive or negative)
as an argument. This function should return the number of digits in x.

Hints:
How to Move Digit-to-digit?
What happens when you divide an integer by 10?
Can you use the whole number part or the remainder?
'''


def ndigits(x):
    ''' return number of digits in x using recursion
    '''
    # convert the number to positive
    num = abs(x)

    # if only 1 digit left return 1
    if num//10 == 0:
        return 1
    # add one for each recursion
    return 1 + ndigits(num//10)

print ndigits(1244)
print ndigits(123)
