def search(L, e):
    ''' Assumes L is a list, the elements of which are in ascending order.
        Returns True if e is in L and False otherwise.
    '''
    
    def b_search(L, e, low, high):
        if high == low:
            # high-low = 0 here, meaning there is nothing left to check
            # just return 'is this value equal to the one we are looking'
            if L[low] == e:
                return 'Found element {0} at position {1}'.format(
                                                            L[low],
                                                            L.index(e))
            else:
                return 'Coudnt find {}'.format(e)
        mid = (low + high)//2
        if L[mid] == e:
                return 'Found element {0} at position {1}'.format(
                                                            L[mid],
                                                            L.index(e))
        elif L[mid] > e:
            # nothing left to search
            if low == mid:
                return 'Couldnt find {}'.format(e)
            else:
                return b_search(L, e, low, mid-1)
        else:
            return b_search(L, e, mid+1, high)
    
    if L:
        return b_search(L, e, 0, len(L) - 1)
    else:
        return False