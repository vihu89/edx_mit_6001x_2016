﻿'''
find whether palindrome
'''
import string


def ispal(s):
    # exclude punctuation, convert to lower case and remove whitespace
    exclude = set(string.punctuation)
    x = ''.join(ch for ch in s.lower() if ch not in exclude).replace(' ', '')

    if len(x) <= 1:
        return True
    return x[0] == x[-1] and ispal(x[1:-1])
