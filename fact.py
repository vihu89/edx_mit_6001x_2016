﻿'''
n! = n * (n-1)!
'''


def fact_iter(n):
    res = 1
    while n > 1:
        res = res*n
        n -= 1
    return res


def fact_rec(n):
    if n == 1:
        return n
    return n*fact_rec(n-1)


print fact_rec(10)
print fact_iter(10)
