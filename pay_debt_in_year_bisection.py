'''
start with some intial upper and lower bounds

loop until we find an answer
    -payment is the average of the upper and lower bounds
    -reset balance to original balance
    -for all 12 months
        remove payment from balance
        add monthly interest to balance
    -if ending balance is sufficiently close to zero
        were done
    -if we shoot past zero that means our payment was too high
        so change the upper bound to payment (choosing the lower half of guesses)
    -if we didnt reach zero that means our payment was too low
        so change the lower bound to payment (choosing the higher half of guesses)
'''


balance = 999999
annualInterestRate = 0.18
epsilon = 0.01
mon_interest_rate = annualInterestRate/12.0
low = balance/12.0
high = (balance * (1.0 + mon_interest_rate)**12.0)/12.0
ans = (low + high)/2.0
guess = 1

updated_balance = balance
while abs(updated_balance) >= epsilon:
    print 'guess#: {4} --> low: {0}, high: {1}, ans: {2}, bal: {3}'.\
            format(low, high, ans, updated_balance, guess)
    updated_balance = balance
    guess += 1
    for i in range(12):
        updated_balance = (updated_balance - ans) * (1 + mon_interest_rate)
    if updated_balance == epsilon:
        break
    elif updated_balance > epsilon:
        low = ans
    else:
        high = ans
    ans = (low + high)/2.0

print 'Lowest Payment {}'.format(round(ans, 2))
